# nagios-plugins-tokens

Token management for monitoring plugins, uses oidc-agent as backend.

```buildoutcfg
SYNPOSIS:

OMD[etf]:~$ /usr/lib64/nagios/plugins/refresh_token --help
usage: refresh_token [-h] [--version] [-H HOSTNAME] [-w WARNING] [-c CRITICAL]
                     [-d] [-p PREFIX] [-s SUFFIX] [-t TIMEOUT] [-C COMMAND]
                     [--dry-run] [-o OUTPUT] [--token-config TOKEN_CONFIG]
                     [--aud AUD] [--token-time TOKEN_TIME] [-x TOKEN_OUT]
                     [--scope scope1 [--scope scope2 [...]]]

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -H HOSTNAME, --hostname HOSTNAME
                        Host name, IP Address, or unix socket (must be an
                        absolute path)
  -w WARNING, --warning WARNING
                        Offset to result in warning status
  -c CRITICAL, --critical CRITICAL
                        Offset to result in critical status
  -d, --debug           Specify debugging mode
  -p PREFIX, --prefix PREFIX
                        Text to prepend to ever metric name
  -s SUFFIX, --suffix SUFFIX
                        Text to append to every metric name
  -t TIMEOUT, --timeout TIMEOUT
                        Global timeout for plugin execution
  -C COMMAND, --command COMMAND
                        Nagios command pipe for submitting passive results
  --dry-run             Dry run, will not execute commands and submit passive
                        results
  -o OUTPUT, --output OUTPUT
                        Plugin output format; valid options are nagios,
                        check_mk or passive (via command pipe); defaults to
                        nagios)
  --token-config TOKEN_CONFIG
                        Token configuration file name (usually under .oidc-
                        agent)
  --token-time TOKEN_TIME
                        Token lifetime to request
  -x TOKEN_OUT, --token-out TOKEN_OUT
                        Token output file
  --aud AUD             Tokens will be generated per host with corresponding
                        audience (starting from top-level directory AUD). 
  --scope SCOPES        Use user defined scope(s)
```