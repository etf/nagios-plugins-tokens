%define dir /usr/lib64/nagios/plugins
Summary: Nagios plugins for managing Scientifc Tokens (Scitokens)
Name: nagios-plugins-tokens
Version: 0.1.5
Release: 2%{?dist}
License: ASL 2.0
Group: Network/Monitoring
Source: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch
Requires: python3-nap
Requires: python3-pexpect
Requires: oidc-agent-cli
BuildRequires: python3-setuptools
BuildRequires: /usr/bin/pathfix.py

%description

%prep
%setup -q
pathfix.py -pni "%{__python3} %{py3_shbang_opts}" refresh_token

%build

%install
rm -rf $RPM_BUILD_ROOT
install --directory ${RPM_BUILD_ROOT}%{dir}
install --mode 755 ./refresh_token ${RPM_BUILD_ROOT}%{dir}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{dir}/refresh_token
